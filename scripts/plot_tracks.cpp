/*!
 *  @file     plot_tracks.cpp
 *  @author   Alessio Piucci
 *  @brief    This scripts plot the tracks in a 3D space: amazing!
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"
#include "TEveTrack.h"
#include "TEveElement.h"
#include "TEveViewer.h"
#include "TEveBrowser.h"
#include "TEveManager.h"
#include "TSystem.h"
#include "TObjArray.h"

#include "TEveTrackPropagator.h"

//custom libraries
#include "../include/TFREvent.h"
#include "../include/TFRParticle.h"
#include "../include/TFRHit.h"

using namespace std;

void plot_tracks(std::string inFile_name = "/home/alessio/Dropbox/hands-on_tracking/output/track_test.root",
		 std::string inEventList = "event_list",
		 std::string inTrackList = "track_list"){

  //include the custom dictionary
  gSystem->Load("build/lib/libDICT.so");
    
  //print the imported options 
  std::cout << "inFile_name = " << inFile_name << std::endl;
  std::cout << "inEventList = " << inEventList << std::endl;
  std::cout << "inTrackList = " << inTrackList << std::endl;

  //-------------------------------//
  //  import the simulated events  //
  //-------------------------------//                                                                                                                                                                
  //open the input file
  TFile *inFile = new TFile(inFile_name.c_str(), "read");

  if(inFile == NULL){
    std::cout << "Error: the input file does not exist" << std::endl;
    exit(EXIT_FAILURE);
  }

  //get a clone of the input events
  TFREvents *event_list = (TFREvents*) inFile->Get(inEventList.c_str());

  //check for the existence of the input events
  if(event_list == NULL){
    std::cout << "Error: the input event list does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }

  //get a clone, I don't want to play with the original copy
  event_list = (TFREvents*) event_list->Clone();
  event_list->SetOwner(kTRUE);

  //some printouts
  std::cout << std::endl;
  std::cout << "number of events = " << event_list->GetEntries() << std::endl;
  std::cout << std::endl;

  //-------------------------------//
  //  finally start with the fun!  //
  //-------------------------------//

  //initialize the graphic application
  TEveManager::Create();
  gSystem->ProcessEvents();
  
  //get the first event, I only want to draw it for the moment
  TFREvent *test_event = (TFREvent*) event_list->At(0);

  //get the particles of the current event                                                                                                                                                          
  TFRParticles *particle_list = (TFRParticles*) test_event->GetGenParticles();
  
  std::cout << "num particles = " << particle_list->GetEntries() << std::endl;
  
  //I've no clue why the hell in ROOT there aren't nice implementations of iterators,                                                                                                              
  //but just this stupid way (that I even didn't used at my C course during my bachelor)                                                                                                           
  TIter it_particle((TFRParticles*) particle_list);
  TFRParticle *curr_particle;
  
  //loop over the particles of the event
  while((curr_particle = (TFRParticle*) it_particle.Next())){

    //get the hits of the current particle
    TFRHits hit_list = (TFRHits) curr_particle->GetHits();
    
    std::cout << "num particles = " << particle_list->GetEntries() << std::endl;
    
    //I've no clue why the hell in ROOT there aren't nice implementations of iterators,
    //but just this stupid way (that I even didn't used at my C course during my bachelor)
    TIter it_hit((TFRHits*) &hit_list);
    TFRHit *curr_hit;
    
    while((curr_hit = (TFRHit*) it_hit.Next())){
      
      //set an TEveHit, needed for drawing
      TEveHit *temp_hit = new TEveHit();
      
      temp_hit->fV = curr_hit->GetPosition();
      
      //add the hit to the event
      gEve->AddElement((TEveElement*) temp_hit);
      
    }  //loop over the hits fot the particle
  }  //loop over the particles
  
  gSystem->ProcessEvents();
  gEve->Redraw3D(kFALSE, kTRUE);

  //close the input file
  inFile->Close();
  
  return;
}
