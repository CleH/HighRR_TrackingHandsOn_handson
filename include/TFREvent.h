#ifndef INCLUDE_TFREVENT_H
#define INCLUDE_TFREVENT_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Event class
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOt libraries
#include "TROOT.h"
#include "TObject.h"

//custom libraries
#include "TFRParticle.h"
#include "TFRTrack.h"

using namespace std;

class TFREvent : public TObject {

 public:
  /*! Standard constructor */
  TFREvent() {
    gen_particles = new TFRParticles();
    reco_signal_particle = new TFRParticle();
    reco_tracks = new TFRTracks();
    clusters = new TFRClusters();
  };
  
  /*! Destructor */
  virtual ~TFREvent( ){ };
  
  /*! Set the event ID */
  void SetID(unsigned int _evt_ID){evt_ID = _evt_ID;};

  /*! Set the generated particles of the event */
  void SetGenParticles(TFRParticles *_gen_particles){gen_particles = _gen_particles;};
  
  /*! Set the reconstructed tracks of the event */
  void SetRecoTracks(TFRTracks* _reco_tracks){reco_tracks = _reco_tracks;};

  /*! Add a generated particle to the event */
  inline void AddGenParticle(TFRParticle *particle){gen_particles->Add(particle);};

  /*! Set the reconstructed signal particle */
  inline void AddRecoSignalParticle(TFRParticle *_reco_signal_particle){
    reco_signal_particle = _reco_signal_particle;
  };
  
  /*! Add a reconstructed track to the event */
  inline void AddRecoTrack(TFRTrack *track){reco_tracks->Add(track);};

  /*! Add a detector cluster to the event */
  inline void AddCluster(TFRCluster *cluster){clusters->Add(cluster);};
  
  /*! Get the event ID */
  unsigned int GetID() const {return evt_ID;};

  /*! Get the generated particles */
  TFRParticles* GetGenParticles(){return gen_particles;};

  /*! Get the reconstructed signal particle */
  TFRParticle* GetRecoSignalParticle(){return reco_signal_particle;};
  
  /*! Get the reconstructed tracks */
  TFRTracks* GetRecoTracks(){return reco_tracks;};

  /*! Returns the number of generated particles */
  unsigned int GetNGenParticles(){return gen_particles->GetEntries();};
  
  /*! Returns the number of reconstructed tracks */
  unsigned int GetNRecoTracks(){return reco_tracks->GetEntries();};

  /*! Get the list of clusters of the event */
  TFRClusters* GetClusters(){return clusters;};

  /*! Find and returns a cluster of the event */
  inline TFRCluster* FindCluster(TFRCluster *cluster){

    //this is a very inefficient way, maybe you want to improve it
    //using sorted list of clusters, or assuming clusters sorted by layers
    
    //loop over the clusters of the event
    for(int i_cluster = 0; i_cluster < clusters->GetEntries(); ++i_cluster){
      if(((TFRCluster*) clusters->At(i_cluster))->GetChannelID()
	 == cluster->GetChannelID())
	return ((TFRCluster*) clusters->At(i_cluster));
    }  //loop over the clusters of the event
    
    return NULL;
  };
  
 protected:

 private:

  /*! Event ID */
  unsigned int evt_ID;

  /*! Generated particles of the event (all of them) */
  TFRParticles *gen_particles;
  
  /*! Reconstructed signal particle */
  TFRParticle *reco_signal_particle;
  
  /*! Reconstructed tracks of the event */
  TFRTracks *reco_tracks;

  /*! Detector clusters of the event */
  TFRClusters *clusters;
  
  //magic happens here
  ClassDef(TFREvent, 1)
};

/*! Array of events */
typedef TObjArray TFREvents;

#endif // INCLUDE_TFREVENT_H
