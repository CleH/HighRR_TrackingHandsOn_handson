
This hands-on consists of different steps:
- simulation of events and particles;
- detector simulation, with creation of particle hits and clusters;
- reconstruction;
- fitting;
- reconstruction of a D0 decay.

A final step converts the internal classes used in the simulation into standard TTrees, to easily handle the simulation output.

To run the full simulation, run this command:

```
source runSim.sh
```

Finally, many many thanks to S. Stemmle for having helped to set the simulation!

# Code documentation
You can find a complete documentation of the code [here](http://physi.uni-heidelberg.de/~piucci/HighRR_TFRHandsOn/html/annotated.html). Amazing that some documentation exists, isn'it?

You can update the documentation with the command:

```
doxygen ./include/Doxyfile.in
```

and then navigate it calling your favourite web browser:

```
firefox html/annotated.html
```

# Software pre-requisities
The following software is required to run the simulation:
- gcc compiler >= 4.6, including g++;
- Boost >= 1.40;
- CMake >= 2.8.0;
- ROOT >= 6.*, with OpenGL and MathMore libraries, including support to x11 and the C++11 standard; it's strongly suggested to compile ROOT using gcc/g++.

# Docker image
A docker image is provided, on which it is possible to run the simulation. It is already pre-configured with all the needed software.
You can find it [here](https://gitlab.com/apiucci/HighRR_TrackingHandsOn_handson/container_registry).