/*!
 *  @file      software_test.cpp
 *  @author    Alessio Piucci
 *  @brief     This macro tests almost all ROOT libraries needed in the hands-on.
 *  @return    A root file containing the propagated tracks
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
#include "TFile.h"
#include "TEveTrack.h"
#include "TEveTrackPropagator.h"
#include "TObjArray.h"

//custom libraries
#include "../include/cparser.h"

using namespace std;

/*! print the instructions for this macro */
void print_instructions(){
  std::cout << std::endl;
  std::cout << "Mandatory options: " << std::endl;
  std::cout << "\t-c: config file name" << std::endl;
  std::cout << "\t-o: output file name" << std::endl;
  std::cout << std::endl;
  exit(1);
}

int main(int argc, char **argv){
  
  //--------------------------------------------//
  //  retrieve the job options from the parser  //
  //--------------------------------------------//
  
  CParser cmdline(argc, argv);
  
  //check the parsed options
  if(argc == 1)
    print_instructions();
  else
  {
    if((cmdline.GetArg("-c") == "") || (cmdline.GetArg("-o") == ""))
      print_instructions();
  }
  
  //get the config file name
  std::string configFile_name = cmdline.GetArg("-c");
  std::string outFile_name = cmdline.GetArg("-o");
  
  //print the imported options
  std::cout << std::endl;
  std::cout << "configFile_name = " << configFile_name << std::endl;
  std::cout << "outFile_name = " << outFile_name << std::endl;
  
  //---------------------------------------//
  //  setup the magnetic field propagator  //
  //---------------------------------------//
  
  std::cout << "Setting the B propagator" << std::endl;
  
  TEveTrackPropagator *propagator = new TEveTrackPropagator();
  
  //setup the Runge Kutta stepper
  propagator->SetStepper(TEveTrackPropagator::kRungeKutta);

  //set a uniform magnetic field on x direction
  propagator->SetMagField(0., 10., 0.);
  
  //some other options
  propagator->SetFitDaughters(kFALSE);      //do not follow the daughters particles
  propagator->SetMaxZ(1000.);               //maximum z-coordinate to propagate, cm
  propagator->SetMaxR(100000000);           //maximum bending radius to propagate, cm
  propagator->SetDelta(pow(10., -6.));      //'error' on trajectory reconstruction
  
  
  //------------------------------//
  //  set the track to propagate  //
  //------------------------------//
  
  std::cout << "Setting the track" << std::endl;
  
  //retrieve the track parameters from the config file
  boost::property_tree::ptree configFile;
  boost::property_tree::read_info(configFile_name, configFile);
  
  TEveVectorD vertex(configFile.get<double>("track.vertex.x"),
                     configFile.get<double>("track.vertex.y"),
                     configFile.get<double>("track.vertex.z"));
  TEveVectorD momentum(configFile.get<double>("track.momentum.x"),
                       configFile.get<double>("track.momentum.y"),
                       configFile.get<double>("track.momentum.z"));
  
  //set the track into the RKRoot format:
  //I need it to easily set initial vertex and momentum values
  TEveRecTrackD *track = new TEveRecTrackD();

  track->fV.Set(vertex);     //set the track vertex [cm]
  track->fP.Set(momentum);   //set the track initial momentum [MeV/c?]
  
  
  //set the track into the final format that we need
  TEveTrack* prop_track = new TEveTrack(track, propagator);

  //set the charge
  prop_track->SetCharge(configFile.get<int>("track.charge"));
  
  //initialize the propagator
  prop_track->GetPropagator()->InitTrack(vertex, prop_track->GetCharge());
  
  //propagate the track!
  //prop_track->MakeTrack();

  
  //-----------------------------//
  //  set the detector geometry  //
  //-----------------------------//

  //retrieve the plane position from the config file
  TEveVectorD plane_position(configFile.get<double>("layer.position.x"),
			     configFile.get<double>("layer.position.y"),
			     configFile.get<double>("layer.position.z"));

  //retrieve the Euler angles from the config file
  double phi = configFile.get<double>("layer.Euler_angles.phi");
  double theta = configFile.get<double>("layer.Euler_angles.theta");
  double psi = configFile.get<double>("layer.Euler_angles.psi");

  //convert the angles, which are currently in degrees, into radians
  phi *= (M_PI / 180.);
  theta *= (M_PI / 180.);
  psi *= (M_PI / 180.);
  
  //compute the rotation matrix in the cartesian coordinate system
  double rot_matrix[3][3];
  
  rot_matrix[0][0] = cos(psi)*cos(phi) -cos(theta)*sin(phi)*sin(psi);
  rot_matrix[1][0] = - sin(psi)*cos(phi) -cos(theta)*sin(phi)*cos(psi);
  rot_matrix[2][0] = sin(theta)*sin(phi);
  rot_matrix[0][1] = cos(psi)*sin(phi) + cos(theta)*cos(phi)*sin(psi);
  rot_matrix[1][1] = -sin(psi)*sin(phi) + cos(theta)*cos(phi)*cos(psi);
  rot_matrix[2][1] = -sin(theta)*cos(phi);
  rot_matrix[0][2] = sin(psi)*sin(theta);
  rot_matrix[1][2] = cos(psi)*sin(theta);
  rot_matrix[2][2] = cos(theta);
  
  //compute the versor orthogonal to the plane
  TEveVectorD plane_ortversor(rot_matrix[2][0], rot_matrix[2][1], rot_matrix[2][2]);
  
  
  //-----------------------------------------------//
  //  compute the intersection with a given plane  //
  //-----------------------------------------------//
  
  //intersection point [cm]
  TEveVectorD intersection;
  
  //helicoidal propagation
  prop_track->GetPropagator()->IntersectPlane(prop_track->GetMomentum(),
					      plane_position, plane_ortversor,
					      intersection);
  
  std::cout << "--> Track propagation completed!" << std::endl;
  std::cout << "intersection point = (" << intersection[0]
	    << ", " << intersection[1]
	    << ", " << intersection[2] << ") cm" << std::endl;
  std::cout << std::endl;

  /*
  //--------------------------------------//
  //  save the tracks in the output file  //
  //--------------------------------------//

  TFile *outFile = new TFile(outFile_name.c_str(), "recreate");
  
  TObjArray *obj_array = new TObjArray();
  obj_array->SetOwner(kTRUE);
  
  outFile->cd();
  
  //add the track to the track list
  obj_array->Add(prop_track);

  //write the track list in the output file
  outFile->WriteObject(obj_array, "track_list");
  //obj_array->Write("track_list", TObject::kSingleKey);

  //write the output file
  outFile->ls();
  outFile->Write();

  //clean the memory
  delete outFile;
  */
  
  return 0;

}
